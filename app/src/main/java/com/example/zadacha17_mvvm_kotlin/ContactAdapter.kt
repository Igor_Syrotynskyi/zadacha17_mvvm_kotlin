package com.example.zadacha17_mvvm_kotlin

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.zadacha17_mvvm_kotlin.ContactAdapter.ContactHolder
import java.util.*

/**
 * Adapter for RecyclerView class
 */
class ContactAdapter : RecyclerView.Adapter<ContactHolder>() {
    private var contacts: List<Contact> = ArrayList()

    /**
     * This method get list of contacts which should to show in Recycler View
     * @param contacts - list of contacts
     */
    fun setContacts(contacts: List<Contact>) {
        this.contacts = contacts
        notifyDataSetChanged() //method to refreshing recycler view
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.contact_item, parent, false)
        return ContactHolder(itemView)
    }

    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
        val contact = contacts[position]
        holder.nameTextView.text = contact.name
        holder.lastNameTextView.text = contact.lastName
        holder.phoneNumberTextView.text = contact.phoneNumber.toString()
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    class ContactHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: TextView = itemView.findViewById(R.id.nameTextView)
        val lastNameTextView: TextView = itemView.findViewById(R.id.lastNameTextView)
        val phoneNumberTextView: TextView = itemView.findViewById(R.id.phoneNumberTextView)

    }
}
