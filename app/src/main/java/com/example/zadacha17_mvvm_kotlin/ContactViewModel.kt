package com.example.zadacha17_mvvm_kotlin

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

/**
 * View Model class
 */
class ContactViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: ContactRepository = ContactRepository(application)

    /**
     * This method insert contact to the database through ContactRepository class.
     *
     * @param contact - the contact which will be added to database
     */
    fun insertContact(contact: Contact) {
        repository.insertContact(contact)
    }

    /**
     * This method get Live Data list with have the searching name
     * @param searchingName - name which we should to find in database
     * @return list of contacts which required name
     */
    fun findContactByName(searchingName: String): LiveData<List<Contact>> {
        return repository.findContactByName(searchingName)
    }

    /**
     * This variable get Live Data list with all contacts in database through ContactRepository class
     */
    val allContacts: LiveData<List<Contact>>
        get() = repository.allContacts

    /**
     * This variable get Live Data information about count of contacts in database
     */
    val countOfContacts: LiveData<Int>
        get() = repository.countOfContacts

    /**
     * This method deletes the contacts from database through ContactRepository class by name entering.
     *
     * @param nameOfDeleting - variable with the name, which contact will be deleted
     */
    fun deleteByName(nameOfDeleting: String) {
        repository.deleteByName(nameOfDeleting)
    }

    /**
     * This method erase all contacts from database through ContactRepository class.
     */
    fun deleteAllContacts() {
        repository.deleteAllContacts()
    }
}
