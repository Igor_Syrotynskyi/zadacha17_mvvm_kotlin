package com.example.zadacha17_mvvm_kotlin

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * This is interface for Contact database
 */
@Dao
interface ContactDao {
    //inserting contact
    @Insert
    fun insertContact(contact: Contact)

    //searching contacts by name in database
    @Query("SELECT * FROM contact_table WHERE name LIKE '%' || :searchingName || '%' ")
    fun getContactsByName(searchingName: String): LiveData<List<Contact>>

    //getting all contacts from database
    @get:Query("SELECT * FROM contact_table")
    val allContacts: LiveData<List<Contact>>

    //count of contacts in database
    @get:Query("SELECT COUNT(name) FROM contact_table")
    val countOfContacts: LiveData<Int>

    //deleting contact by name from database
    @Query("DELETE FROM contact_table WHERE name LIKE :nameOfDeleting")
    fun deleteByNameContact(nameOfDeleting: String)

    //deleting all contacts from database
    @Query("DELETE FROM contact_table")
    fun clearAllDataBase()
}
