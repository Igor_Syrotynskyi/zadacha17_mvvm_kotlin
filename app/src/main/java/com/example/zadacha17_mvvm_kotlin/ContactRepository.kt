package com.example.zadacha17_mvvm_kotlin

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.zadacha17_mvvm_kotlin.ContactDatabase.Companion.getInstance

/**
 * Repository class
 */
class ContactRepository(application: Application) {
    private val contactDao: ContactDao

    /**
     * This method insert contact to the database
     *
     * @param contact - the contact which will be added to database
     */
    fun insertContact(contact: Contact) {
        InsertContactAsyncTask(contactDao).execute(contact)
    }

    /**
     * This method get Live Data list with have the searching name
     *
     * @param searchingName - name which we should to find in database
     * @return list of contacts which required name
     */
    fun findContactByName(searchingName: String): LiveData<List<Contact>> {
        return contactDao.getContactsByName(searchingName)
    }

    /**
     * This method get Live Data list with all contacts in database
     *
     * @return the List with all contacts
     */
    val allContacts: LiveData<List<Contact>>
        get() = contactDao.allContacts

    /**
     * This method get Live Data information about count of contacts in database
     *
     * @return count of contacts
     */
    val countOfContacts: LiveData<Int>
        get() = contactDao.countOfContacts

    /**
     * This method deletes the contacts from database
     *
     * @param nameOfDeleting - variable with the name, which contact will be deleted
     */
    fun deleteByName(nameOfDeleting: String) {
        DeleteByNameAsyncTask(contactDao).execute(nameOfDeleting)
    }

    /**
     * This method erase all contacts from database.
     */
    fun deleteAllContacts() {
        DeleteAllContactsAsyncTask(contactDao).execute()
    }

    private class InsertContactAsyncTask(private val contactDao: ContactDao) : AsyncTask<Contact, Void, Void>() {
        protected override fun doInBackground(vararg contacts: Contact): Void? {
            contactDao.insertContact(contacts[0])
            return null
        }
    }

    private class DeleteByNameAsyncTask(private val contactDao: ContactDao) : AsyncTask<String, Void, Void>() {
        protected override fun doInBackground(vararg strings: String): Void? {
            contactDao.deleteByNameContact(strings[0])
            return null
        }
    }

    private class DeleteAllContactsAsyncTask(private val contactDao: ContactDao) : AsyncTask<Void, Void, Void>() {
        protected override fun doInBackground(vararg voids: Void): Void? {
            contactDao.clearAllDataBase()
            return null
        }
    }

    /**
     * Constructor for the class where getting instance of database, variable of Dao and LiveData list of contacts from database
     *
     * @param aplication
     */
    init {
        val database = getInstance(application)
        contactDao = database.contactDao()
    }
}
