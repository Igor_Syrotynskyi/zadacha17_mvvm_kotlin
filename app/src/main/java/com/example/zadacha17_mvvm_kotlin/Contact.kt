package com.example.zadacha17_mvvm_kotlin

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * This class describes a database entity.
 */
@Entity(tableName = "contact_table") //The template for Contact objects.
data class Contact (
        val name: String,
        val lastName: String,
        val phoneNumber: Int,
        @PrimaryKey(autoGenerate = true)
        var id :Int = 0
       )
