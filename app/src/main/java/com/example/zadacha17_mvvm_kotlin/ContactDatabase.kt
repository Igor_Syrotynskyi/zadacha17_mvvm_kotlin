package com.example.zadacha17_mvvm_kotlin

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * This is Contact database class
 */
@Database(entities = [Contact::class], version = 1)
abstract class ContactDatabase : RoomDatabase() {
    abstract fun contactDao(): ContactDao

    companion object {
        private var instanse: ContactDatabase? = null

        /**
         * This method creates database ONLY ones and return instanse of it.. If database is already created, this method only return instance on it.
         *
         * @param context
         * @return instance of database.
         */
        @Synchronized
        fun getInstance(context: Context): ContactDatabase {
            if (instanse == null) {
                instanse = Room.databaseBuilder(context.applicationContext,
                        ContactDatabase::class.java, "contact_database")
                        .fallbackToDestructiveMigration()
                        .build()
            }
            return instanse!!
        }
    }
}
