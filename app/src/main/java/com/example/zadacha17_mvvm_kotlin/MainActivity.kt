package com.example.zadacha17_mvvm_kotlin

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var contactViewModel: ContactViewModel
    private lateinit var adapter: ContactAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializationAdapter()
        viewModelObservers()
        addClickListenerToButtons()
    }

    /**
     * This method initializes ViewModel variable and created two Live Data observers.
     */
    private fun viewModelObservers() {
        contactViewModel = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.application)).get(ContactViewModel::class.java)
        //Live Data observer on the all contacts in database. Return List<Contact> contacts.
        contactViewModel.allContacts.observe(this, { contacts -> //update adapter
            adapter.setContacts(contacts)
        })

        //Live Data observer on the count of contacts in database. Return Integer countOfContact.
        contactViewModel.countOfContacts.observe(this, { countOfContact -> countOfContactTextView.text = countOfContact.toString() })
    }

    /**
     * This method added click listeners to all buttons
     */
    private fun addClickListenerToButtons() {
        addContactButton.setOnClickListener(this)
        clearButton.setOnClickListener(this)
        searchContactButton.setOnClickListener(this)
        deleteContactButton.setOnClickListener(this)
    }

    /**
     * This method initializes recyclerview and adapter
     */
    private fun initializationAdapter() {
        adapter = ContactAdapter()
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
    }

    /**
     * This method handles the push of a buttons
     *
     * @param v - id of buttons
     */
    override fun onClick(v: View) {
        when (v.id) {
            R.id.addContactButton -> addContact()
            R.id.searchContactButton -> findContact()
            R.id.deleteContactButton -> deleteByName()
            R.id.clearButton -> clearDataBase()
        }
    }

    /**
     * This method reads information from EditText and creates new Contact object.
     *
     * @return new Contact object.
     */
    private fun createContact(): Contact {
        val tempName = nameEditText.text.toString()
        val tempLastName = lastNameEditText.text.toString()
        val tempPhoneNumber = phoneNumberEditText.text.toString().toInt()
        return Contact(tempName, tempLastName, tempPhoneNumber)
    }

    /**
     * This method get Contact object from createContact() method and add it to the database through ContactViewModel class.
     */
    private fun addContact() {
        val newContact = createContact()
        contactViewModel.insertContact(newContact)
        Toast.makeText(this, "The contact " + newContact.name + " was added", Toast.LENGTH_SHORT).show()
    }

    /**
     * This method displays all contacts from database through ContactViewModel class which have the searching name.
     * I know, this is bad practice to create new observe each time when i request it. I will fix it in the next time.
     */
    private fun findContact() {
        //get name which we should to find
        val searchingName = searchContactEditText.text.toString()

        //get list of contacts which required name from database
        contactViewModel.findContactByName(searchingName).observe(this, { contacts ->
            val amount = contacts.size
            //return result
            if (amount == 0) {
                searchingResultTextView.text = "There no contacts with the name"
            } else {
                val namesFromDatabase = StringBuilder()
                for (i in 0 until amount) {
                    val (name) = contacts[i]
                    //creating String with required names
                    namesFromDatabase.append(name).append(" ")
                }
                searchingResultTextView.text = namesFromDatabase.toString()
            }
        })
    }

    /**
     * This method deletes the contacts from database through ContactViewModel class by name entering.
     */
    private fun deleteByName() {
        val deletingContactName = deleteContactEditText.text.toString()
        contactViewModel.deleteByName(deletingContactName)
    }

    /**
     * This method erase all contacts from database through ContactViewModel class.
     */
    private fun clearDataBase() {
        contactViewModel.deleteAllContacts()
        Toast.makeText(this, "The contacts list was erased", Toast.LENGTH_SHORT).show()
    }
}
